# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-trimesh
pkgver=3.21.1
pkgrel=0
pkgdesc="Python library for working with triangular meshes"
url="https://github.com/mikedh/trimesh"
# x86, armhf, armv7 Tests fail on int64 to int32 casts on these arches
# s390x, no py3-rtree
# riscv64, no py3-shapely
arch="noarch !x86 !armhf !armv7 !s390x !riscv64"
license="MIT"
depends="
	py3-colorlog
	py3-jsonschema
	py3-lxml
	py3-mapbox-earcut
	py3-msgpack
	py3-networkx
	py3-numpy
	py3-pillow
	py3-requests
	py3-rtree
	py3-scipy
	py3-shapely
	py3-svgpath
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest py3-pytest-xdist py3-pyinstrument"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikedh/trimesh/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/trimesh-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	# test_obj.py: no format zae, probably needs more investigation
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest -n auto \
		--deselect tests/test_dae.py::DAETest::test_material_round \
		--deselect tests/test_dae.py::DAETest::test_obj_roundtrip \
		--deselect tests/test_light.py::LightTests::test_scene \
		--deselect tests/test_obj.py::OBJTest::test_multi_nodupe
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
08716a310db8cae86a725072a6626b3703ffac3532182ce22389cf85fcdf5853ad8f17cfd8c34afde0e6dc0f2125d910d130810c3cce8e8dcb1605737074a3e0  py3-trimesh-3.21.1.tar.gz
"
