# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php8-pecl-timezonedb
_extname=timezonedb
pkgver=2023.2
pkgrel=0
pkgdesc="Timezone Database to be used with PHP's date and time functions."
url="https://pecl.php.net/package/timezonedb"
arch="all"
license="PHP-3.01"
depends="php8-common"
makedepends="php8-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize8
	./configure --prefix=/usr --with-php-config=php-config8
	make
}

check() {
	# Test suite is not a part of pecl release.
	php8 -dextension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/php8/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/40_$_extname.ini
}

sha512sums="
26f0da970e4af7fcb3bea86aee005c4e19f71608baf51ba3900f0ef9e5e0eec8dbd02843dfdafd7baabf68d4cbba8c652fabe1fd1e4956bb0ba58c7a967a0856  php-pecl-timezonedb-2023.2.tgz
"
