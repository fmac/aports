# Maintainer: Jeff Dickey <alpine@rtx.pub>
pkgname=rtx
pkgver=1.27.8
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://rtx.pub"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdxcode/rtx/archive/refs/tags/v$pkgver.tar.gz"

export CARGO_REGISTRIES_CRATES_IO_PROTOCOL=sparse

prepare() {
	default_prepare
	git init .
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --all-features --frozen
}

package() {
	install -Dm755 target/release/rtx \
		-t "$pkgdir"/usr/bin/
}

sha512sums="
b98251c29fd4e0d83c71ac66b10683fe66bc8deb37f18e1c51389f8a7a3c2af015a40f6c4931549b00db401beee0f4d87ddf7ffa422d27059f9a4f0fda8ceaac  rtx-1.27.8.tar.gz
"
