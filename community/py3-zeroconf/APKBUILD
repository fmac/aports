# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-zeroconf
pkgver=0.47.4
pkgrel=0
pkgdesc="Python implementation of multicast DNS service discovery"
url="https://github.com/jstasiak/python-zeroconf"
arch="all"
license="LGPL-2.0-or-later"
replaces="py-zeroconf" # for backwards compatibility
provides="py-zeroconf=$pkgver-r$pkgrel" # for backwards compatibility
depends="python3 py3-ifaddr"
makedepends="
	cython
	py3-gpep517
	py3-poetry-core
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-asyncio"
source="python-zeroconf-$pkgver.tar.gz::https://github.com/jstasiak/python-zeroconf/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir"/python-zeroconf-$pkgver

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
2b8438e0832cfc6ac40d54eaed75749389fd9287b82b074f129bba76e7f31e3cfd58e5de28c54b3cf3e14d3c8540ff09b5f10d85f705ede2ef18718b1a87ebf1  python-zeroconf-0.47.4.tar.gz
"
